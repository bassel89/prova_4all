package prova_4all.testes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Desafio2 {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/chromedriver.exe");
		driver = new ChromeDriver();
		baseUrl = "https://www.google.com/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testDesafio2() throws Exception {
		driver.get("https://shopcart-challenge.4all.com/");
		driver.findElement(By.id("open-categories-btn")).click();
		driver.findElement(By.id("category-0")).click();
		driver.findElement(By.id("add-product-0-btn")).click();
		driver.findElement(By.id("add-product-1-btn")).click();
		Thread.sleep(10000);

		driver.findElement(By.id("add-product-2-btn")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("open-categories-btn")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("category-all")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("add-product-3-btn")).click();
		driver.findElement(By.xpath("//button[@id='cart-btn']/img")).click();
		for (int j = 1; j <= 8; j++) {
			driver.findElement(By.xpath("//div[@id='add-product-3-qtd']/span")).click();
			Thread.sleep(1000);
		}
		for (int j = 1; j <= 4; j++) {
			driver.findElement(By.xpath("//div[@id='remove-product-3-qtd']/span")).click();
			Thread.sleep(1000);
		}
		assertEquals("R$ 36,00",driver.findElement(By.id("subtotal-price")).getText());
		System.out.println("o valor total �: "+driver.findElement(By.id("subtotal-price")).getText());
		driver.findElement(By.xpath("//div[@id='root']/div[2]")).click();
		driver.findElement(By.id("finish-checkout-button")).click();
		driver.findElement(By.xpath("//div[@id='root']/div[3]/div/div/div/h2")).click();
		assertEquals("Pedido realizado com sucesso!",
				driver.findElement(By.xpath("//div[@id='root']/div[3]/div/div/div/h2")).getText());
		driver.findElement(By.xpath("//div[@id='root']/div[3]/div/div/div/button")).click();
		driver.close();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
